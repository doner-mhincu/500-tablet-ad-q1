/**
 * @author alteredq / http://alteredqualia.com/
 * @author mr.doob / http://mrdoob.com/
 */

var Detector = {

	canvas: !! window.CanvasRenderingContext2D,
	webgl: ( function () { try { var canvas = document.createElement( 'canvas' ); return !! ( window.WebGLRenderingContext && ( canvas.getContext( 'webgl' ) || canvas.getContext( 'experimental-webgl' ) ) ); } catch( e ) { return false; } } )(),
	workers: !! window.Worker,
	fileapi: window.File && window.FileReader && window.FileList && window.Blob,

	getWebGLErrorMessage: function () {

		var element = document.createElement( 'div' );
		element.id = 'webgl-error-message';
		element.style.fontFamily = 'font';
		element.style.fontSize = '33px';
		element.style.fontWeight = 'normal';
		element.style.textAlign = 'center';
		element.style.background = '#000';
		element.style.color = '#fff';
		element.style.paddingTop = '6em';
		element.style.width = '100%';
		element.style.height = '100%';
		element.style.margin = '0 auto 0';
		element.style.position = 'fixed';
		element.style.top = '0';

		if (  !this.webgl ) {

			element.innerHTML = window.WebGLRenderingContext ? [
				'OOPS!<br />',
				'<br />',
				'your browser does not<br />',
				'support webgl.<br />',
				'<br />',
				'<a href="http://get.webgl.org/" style="color:#900123">click here</a> to find<br />',
				'out more'
			].join( '\n' ) : [
				'OOPS!<br />',
				'<br />',
				'your browser does not<br />',
				'support webgl.<br />',
				'<br />',
				'<a href="http://get.webgl.org/" style="color:#900123">click here</a> to find<br />',
				'out more'
			].join( '\n' );

		}

		return element;

	},

	addGetWebGLMessage: function ( parameters ) {

		var parent, id, element;

		parameters = parameters || {};

		parent = parameters.parent !== undefined ? parameters.parent : document.body;
		id = parameters.id !== undefined ? parameters.id : 'oldie';

		element = Detector.getWebGLErrorMessage();
		element.id = id;

		parent.appendChild( element );

	}

};

// browserify support
if ( typeof module === 'object' ) {

	module.exports = Detector;

}
