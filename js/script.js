$(document).ready(function() {
    $('#form-fiat').submit(function() {
            $sendNow = true;
            $toSend = {
                brand: 'Fiat',
                mediaCode: '3921',
                title: $('[name=title]').val()
            };
            $models = [];
            $('#form-fiat input[type=text]').each(function(i, e) {
                if ($(e).val() == '') {
                    $sendNow = false;
                }
                if ($(e).attr('name') !== 'email_confirmation') {
                    $toSend[$(e).attr('name')] = $(e).val();
                }
            });
            $('#form-fiat input[type=radio]').each(function(i, e) {
                if ($(e).prop('checked')) {
                    if ($(e).attr('name') == "modelname") {
                        $models.push($(e).val());
                    } else if ($(e).attr('name') == "requesttype") {
                        $toSend['requesttype'] = $(e).val();
                    } else {
                        $toSend[$(e).attr('name')] = 'Y';
                    }
                } else {
                    if ($(e).attr('name') !== "modelname" && $(e).attr('name') !== "requesttype") {
                        $toSend[$(e).attr('name')] = 'N';
                    }
                }
            });
            if(!$toSend['requesttype']){
                $sendNow = false;
                errorMessage();
            }
            $checked = true;
            $.each([$toSend['oPost'], $toSend['oTel'], $toSend['oEmail']], function(index, val) {
                if(val == 'Y'){
                    $checked = true;
                }
            });

            if(!$checked){
                $sendNow = false;
            }
            if(!$models.length){
                $sendNow = false;
                errorMessage();
            }
            else{
                $.each($models, function(index, val) {
                    nIndex = index+1;
                    $toSend['modelName'+nIndex] = val;
                    $toSend['modelCode'+nIndex] = 0;
                });
            }
            if ($sendNow) {
                $.ajax({
                    url: 'http://tdhtaap.doner451.co.uk/taap.php?debug=1&type=' + $toSend.requesttype,
                    type: 'POST',
                    dataType: "xml",
                    data: $toSend,
                    success: function(data) {
                        successMessage();
                        console.log(data);
                    }
                });
            } else {
                errorMessage();
            }
            return false;
        })

        function errorMessage() {
            var message = '<img src="img/error.png">';
            $('.modal-message .message-form').addClass('error').html(message);
            $('.modal-message').fadeIn();
        }

        function successMessage() {
            var message = '<img src="img/thank.png">';
            $('.modal-message .message-form').removeClass('error').html(message);
            $('.modal-message').fadeIn();
        }
        $('.modal-message .btn-danger').click(function(){
            $('.modal-message').fadeOut();
        })
});