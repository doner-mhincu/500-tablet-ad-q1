var app = angular.module('app', ['ngRoute']);

app.config(['$routeProvider', function($routes) {

  $routes.when('/',{
    templateUrl : 'views/home.html',
     controller : 'HomeController'
  });

  $routes.when('/exterior',{
    templateUrl : 'views/exterior.html',
     controller : 'ExteriorController'
  });

  $routes.when('/interior',{
    templateUrl : 'views/interior.html'
    //controller : 'InteriorController'
  });

  $routes.when('/gallery',{
    templateUrl : 'views/gallery.html',
    // controller : 'GalleryController'
  });

  $routes.when('/form',{
    templateUrl : 'views/form-section.html'
    // controller : 'FormController'
  });

  $routes.when('/live-preview',{
    templateUrl : 'views/live-preview.html'
  });

  $routes.otherwise({
    redirectTo : '/'
  });

}]);

app.directive('headerSection', function(){
	return {
		restrict: 'E',
		templateUrl: 'views/header-section.html'
	};
});
app.directive('footerNav', function(){
	return {
		restrict: 'E',
		templateUrl: 'views/footer-nav.html'
	};
});
app.directive('buttonSection', function(){
  return {
    restrict: 'E',
    templateUrl: 'views/button-section.html'
  };
});

 app.controller('HomeController', function($scope) {
  console.log('HOME');
  removeWebGlMessage();
 });

 app.controller('ExteriorController', function($scope) {
  console.log('EXTERIOR');
  removeWebGlMessage();
 });

 function removeWebGlMessage()
 {
  var elem = document.getElementById("oldie");
  if (elem)
  {
    elem.parentNode.removeChild(elem);
  }  
 }

app.controller('InteriorController', function() {
  
      removeWebGlMessage();

      if ( ! Detector.webgl ) {
        Detector.addGetWebGLMessage();
      }
      else
      {
        var container, camera, scene, renderer, controls, geometry, mesh, backgroundButton;

        var animate = function(){

          window.requestAnimationFrame( animate );

          controls.update();
          renderer.render(scene, camera);

        };

        container = document.getElementById( 'container3d' );

        camera = new THREE.PerspectiveCamera(80, window.innerWidth / window.innerHeight, 1, 1100);

        controls = new THREE.DeviceOrientationControls( camera );

        scene = new THREE.Scene();
        
        $("#preloader").hide();

        var geometry = new THREE.SphereGeometry( 500, 316, 18 );
        geometry.applyMatrix( new THREE.Matrix4().makeScale( -1, 1, 1 ) );

      
        var material = new THREE.MeshBasicMaterial( {
          map: THREE.ImageUtils.loadTexture( 'img/image.jpg' )
        } );

        var mesh = new THREE.Mesh( geometry, material );
        scene.add( mesh );

        var geometry = new THREE.BoxGeometry( 100, 100, 100, 4, 4, 4 );
        var mesh = new THREE.Mesh( geometry, material );
        scene.add( mesh );

        renderer = new THREE.WebGLRenderer();
        renderer.setSize(window.innerWidth, window.innerHeight);
        renderer.domElement.style.position = 'relative';
        renderer.domElement.style.top = 0;
        container.appendChild(renderer.domElement);

         // TODO: change this to angular equivalent
        window.addEventListener('resize', function() {
          camera.aspect = window.innerWidth / window.innerHeight;
          camera.updateProjectionMatrix();
          renderer.setSize( window.innerWidth, window.innerHeight );

        }, false);

        animate();

        console.log('INTERIOR BEGIN');
      }            

});

// TODO MARIUS - maybe split these controllers out into their own .js filea?
app.controller('GalleryController', function() { 

  removeWebGlMessage();

  var gallery = $('#gallery'),
  gallery1 = $('#gallery1');

  console.log("init gallery");
  
  $('#gallery').photobox('a', {
      thumbs: true,
      loop: false
  }, callback);

  $('#gallery1').photobox('a', {
      thumbs: true,
      loop: false
  }, callback);

  function callback(){
      console.log('image has been loaded');
  }

   console.log("GALLERY CONTROLLER BEGIN");

});

app.controller('FormController', function($scope) {

  $('#form-fiat').submit(function() {
            $sendNow = true;
            $toSend = {
                brand: 'Fiat',
                mediaCode: '3921',
                title: $('[name=title]').val()
            };
            $models = [];
            $('#form-fiat input[type=text]').each(function(i, e) {
                if ($(e).val() == '') {
                    $sendNow = false;
                }
                if ($(e).attr('name') !== 'email_confirmation') {
                    $toSend[$(e).attr('name')] = $(e).val();
                }
            });
            $('#form-fiat input[type=radio]').each(function(i, e) {
                if ($(e).prop('checked')) {
                    if ($(e).attr('name') == "modelname") {
                        $models.push($(e).val());
                    } else if ($(e).attr('name') == "requesttype") {
                        $toSend['requesttype'] = $(e).val();
                    } else {
                        $toSend[$(e).attr('name')] = 'Y';
                    }
                } else {
                    if ($(e).attr('name') !== "modelname" && $(e).attr('name') !== "requesttype") {
                        $toSend[$(e).attr('name')] = 'N';
                    }
                }
            });
            if(!$toSend['requesttype']){
                $sendNow = false;
                errorMessage();
            }
            $checked = true;
            $.each([$toSend['oPost'], $toSend['oTel'], $toSend['oEmail']], function(index, val) {
                if(val == 'Y'){
                    $checked = true;
                }
            });

            if(!$checked){
                $sendNow = false;
            }
            if(!$models.length){
                $sendNow = false;
                errorMessage();
            }
            else{
                $.each($models, function(index, val) {
                    nIndex = index+1;
                    $toSend['modelName'+nIndex] = val;
                    $toSend['modelCode'+nIndex] = 0;
                });
            }
            if ($sendNow) {
                $.ajax({
                    url: 'http://tdhtaap.doner451.co.uk/taap.php?debug=1&type=' + $toSend.requesttype,
                    type: 'POST',
                    dataType: "xml",
                    data: $toSend,
                    success: function(data) {
                        successMessage();
                        console.log(data);
                    }
                });
            } else {
                errorMessage();
            }
            return false;
        })

        function errorMessage() {
            var message = '<img src="img/error.png">';
            $('.modal-message .message-form').addClass('error').html(message);
            $('.modal-message').fadeIn();
        }

        function successMessage() {
            var message = '<img src="img/thank.png">';
            $('.modal-message .message-form').removeClass('error').html(message);
            $('.modal-message').fadeIn();
        }
        $('.modal-message .btn-danger').click(function(){
            $('.modal-message').fadeOut();
        })

});